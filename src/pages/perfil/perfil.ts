import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { EditPerfilPage } from '../edit-perfil/edit-perfil';
import { SettingsPage } from '../settings/settings';
import { AnunciosPage } from '../anuncios/anuncios';
import { AjudaPage } from '../ajuda/ajuda';
import { SobrePage } from '../sobre/sobre';

/**
 * Generated class for the PerfilPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-perfil',
  templateUrl: 'perfil.html',
})
export class PerfilPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PerfilPage');
  }

  editPerfil(){
    this.navCtrl.push(EditPerfilPage.name);
  }

  settings(){
    this.navCtrl.push(SettingsPage.name);
  }

  anuncios(){
    this.navCtrl.push(AnunciosPage.name);
  }

  ajuda(){
    this.navCtrl.push(AjudaPage.name);
  }

  sobre(){
    this.navCtrl.push(SobrePage.name);
  }

}
