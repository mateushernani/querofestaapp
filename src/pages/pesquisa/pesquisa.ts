import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { PesquisaDoisPage } from '../pesquisa-dois/pesquisa-dois';

@IonicPage()
@Component({
  selector: 'page-pesquisa',
  templateUrl: 'pesquisa.html',
})
export class PesquisaPage {

  active: boolean = false;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PesquisaPage');
  }

  loadSegment(){
    //this.active = true;
    this.navCtrl.push(PesquisaDoisPage);
  }

  // backFalse(){
  //   this.active = false;
  // }

}
