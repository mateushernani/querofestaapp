import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-mudar-senha',
  templateUrl: 'mudar-senha.html',
})
export class MudarSenhaPage {

  valida: boolean = false;
  showPwd: boolean = false;
  showPdwNew: boolean = false;
  showPdwNewConf: boolean = false;
  passwordAtual: string;
  passwordNovo: string;
  passwordNovoConf: string;

  constructor(public navCtrl: NavController, public navParams: NavParams, public toastCtrl: ToastController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MudarSenhaPage');
  }

  validaSenha() {
    let toast = this.toastCtrl.create({ duration: 3000, position: 'bottom' });
    if (this.passwordAtual == '123') {
      this.valida = true;
    } else {
      toast.setMessage('Senha incorreta!');
      toast.present();
    }
  }

  trocarSenha() {
    let toast = this.toastCtrl.create({ duration: 3000, position: 'bottom' });
    if (this.passwordNovoConf != this.passwordNovo) {
      toast.setMessage('Senhas diferentes!');
      toast.present();
    }
  }

}
