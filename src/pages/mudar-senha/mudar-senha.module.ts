import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MudarSenhaPage } from './mudar-senha';

@NgModule({
  declarations: [
    MudarSenhaPage,
  ],
  imports: [
    IonicPageModule.forChild(MudarSenhaPage),
  ],
  exports: [
    MudarSenhaPage
  ]
})
export class MudarSenhaPageModule {}
