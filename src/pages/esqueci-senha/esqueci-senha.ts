import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { TabsPage } from '../tabs/tabs';

/**
 * Generated class for the CadastroEmailPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-esqueci-senha',
  templateUrl: 'esqueci-senha.html',
})
export class EsqueciSenhaPage {
  alertCtrl: any;

  @ViewChild('Telefone') Telefone;
  @ViewChild('CPF') CPF;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad EsqueciSenhaPage');
  }

 Confirmar(){
    if(this.Telefone.value == '123' && this.CPF.value == '456'){
      this.navCtrl.push(TabsPage);
    } else {
      this.showAlert();
    }
  }

  showAlert(){
    const alert = this.alertCtrl.create({
      title: 'Informações incompatíveis',
      subTitle: 'Não existe usuário cadastro com os dados informados',
      buttons: ['OK']
    });
    alert.present();
  }

}
