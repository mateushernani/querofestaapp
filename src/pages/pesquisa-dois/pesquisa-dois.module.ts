import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PesquisaDoisPage } from './pesquisa-dois';

@NgModule({
  declarations: [
    PesquisaDoisPage,
  ],
  imports: [
    IonicPageModule.forChild(PesquisaDoisPage),
  ],
  exports: [
    PesquisaDoisPage
  ]
})
export class PesquisaTestePageModule {}
