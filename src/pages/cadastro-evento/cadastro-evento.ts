import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController } from 'ionic-angular';
import { Http } from '@angular/http';
import { FileUploadOptions } from '@ionic-native/file-transfer';
import { Camera, CameraOptions } from '@ionic-native/camera';
import { TransferObject, Transfer } from '@ionic-native/transfer';

@IonicPage()
@Component({
  selector: 'page-cadastro-evento',
  templateUrl: 'cadastro-evento.html',
})
export class CadastroEventoPage {

  evento: any = {};
  estilos: any = {};
  valueEstilos: string = "";
  imageURI: any;
  imageFileName: any;

  constructor(public navCtrl: NavController, public navParams: NavParams, public http: Http, private transfer: Transfer, private camera: Camera, public toastCtrl: ToastController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CadastroEventoPage');
  }

  salvarEvento(){
    this.evento.action = "insert";
    this.http.post("http://192.168.0.111/inserir_evento.php", this.evento).subscribe(data => {
      console.log(data);
      // this.navCtrl.pop();
      // let result = JSON.parse(data["_body"]);
    }, err => {
      console.log(err);
    });
  }

  getImage(){
    const options: CameraOptions = {
      quality: 70,
      destinationType: this.camera.DestinationType.DATA_URL,
      sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
      saveToPhotoAlbum: false
    }

    this.camera.getPicture(options).then((imageData) => {
      this.imageURI = 'data:image/jpeg;base64,' + imageData;
    }, (err) => {
      console.log(err);
    });

    this.imageFileName = "myImage.jpg"
  }

  //faz o upload da imagem
  uploadImage() {

    //create file transfer object
    const fileTransfer: TransferObject = this.transfer.create();

    //random int


    //option transfer
    let options: FileUploadOptions = {
      fileKey: 'photo',
      chunkedMode: false,
      fileName: this.imageFileName,
      httpMethod: 'post',
      mimeType: "image/jpeg",
      headers: {}
    }


    //file transfer action
    fileTransfer.upload(this.imageURI, 'http://192.168.0.111/ionx/iimagem.php', options) //link direcionando a insert de imagem no servidor
      .then((data) => {

      }, (err) => {
        console.log(err);
        let toast = this.toastCtrl.create({position: "bottom", duration: 5000, message: err});
        toast.present();

      });
  }

  presentToast(msg){
    let toast = this.toastCtrl.create({
      message: msg,
      duration: 3000,
      position: 'bottom'
    });

    toast.onDidDismiss(() => {
      console.log("Dismissed toast");
    });

    toast.present();
  }

  change(value){
    this.valueEstilos = this.valueEstilos + value + ",";
  }

}
