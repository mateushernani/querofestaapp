import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { CadastroEmailPage } from '../cadastro-email/cadastro-email';
import { EsqueciSenhaPage } from '../esqueci-senha/esqueci-senha';
//import { HomePage } from '../home/home';
import { TabsPage } from '../tabs/tabs';

@IonicPage()
@Component({
  selector: 'page-login-email',
  templateUrl: 'login-email.html',
})
export class LoginEmailPage {

  @ViewChild('email') email;
  @ViewChild('senha') senha;

  constructor(public navCtrl: NavController, public navParams: NavParams, public alertCtrl: AlertController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginEmailPage');
  }

  cadastroEmail(){
    this.navCtrl.push(CadastroEmailPage);
  }

  esqueciSenha(){
    this.navCtrl.push(EsqueciSenhaPage);
  }

  logarEmail(){
    if(this.email.value == 'teste' && this.senha.value == '123'){
      this.navCtrl.push(TabsPage.name);
    } else {
      this.showAlert();
    }
  }

  showAlert(){
    const alert = this.alertCtrl.create({
      title: 'Ops!',
      subTitle: 'O login e senha digitado é inválido. Por favor, digite as informações corretas ou vá em Esqueci a senha.',
      buttons: ['OK']
    });
    alert.present();
  }

}
