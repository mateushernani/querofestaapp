import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { CadastroEventoPage } from '../cadastro-evento/cadastro-evento';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  event: string = "eventos";

  constructor(public navCtrl: NavController) {

  }

  cadastrarEvento(){
    this.navCtrl.push(CadastroEventoPage.name);
  }

}
