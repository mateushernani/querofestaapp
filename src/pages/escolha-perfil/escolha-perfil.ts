import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { LoginPage } from '../login/login';

@IonicPage()
@Component({
  selector: 'page-escolha-perfil',
  templateUrl: 'escolha-perfil.html',
})
export class EscolhaPerfilPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad EscolhaPerfilPage');
  }

  login(){
    this.navCtrl.setRoot(LoginPage.name);
  }

}
