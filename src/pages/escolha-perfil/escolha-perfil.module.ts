import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { EscolhaPerfilPage } from './escolha-perfil';

@NgModule({
  declarations: [
    EscolhaPerfilPage,
  ],
  imports: [
    IonicPageModule.forChild(EscolhaPerfilPage),
  ],
  exports: [
    EscolhaPerfilPage
  ]
})
export class EscolhaPerfilPageModule {}
