import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { LoginPage } from '../login/login';
import { MudarSenhaPage } from '../mudar-senha/mudar-senha';

@IonicPage()
@Component({
  selector: 'page-settings',
  templateUrl: 'settings.html',
})
export class SettingsPage {

  constructor(public navCtrl: NavController, public navParams: NavParams, public alertCtrl: AlertController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SettingsPage');
  }

  limparHistorico() {
    const confirm = this.alertCtrl.create({
      //title: 'Use this lightsaber?',
      message: 'Tem certeza que deseja limpar o histórico?',
      buttons: [
        {
          text: 'Sim',
          handler: () => {
            console.log('Sim clicado');
          }
        },
        {
          text: 'Não',
          handler: () => {
            console.log('Não clicado');
          }
        }
      ]
    });
    confirm.present();
  }

  sair(){
    this.navCtrl.setRoot(LoginPage.name);
  }

  changePassword(){
    this.navCtrl.push(MudarSenhaPage.name);
  }
}