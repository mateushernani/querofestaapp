import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { FileTransfer } from '@ionic-native/file-transfer';
import { File } from '@ionic-native/file';
import { Camera } from '@ionic-native/camera';
import { Transfer } from '@ionic-native/transfer';

import { HomePage } from '../pages/home/home';
import { CadastroEmailPage } from '../pages/cadastro-email/cadastro-email';
import { EsqueciSenhaPage } from '../pages/esqueci-senha/esqueci-senha';

import { MyApp } from './app.component';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { AngularFireModule } from '@angular/fire';
import { InputMaskModule } from 'ionic-input-mask';
import { PesquisaDoisPage } from '../pages/pesquisa-dois/pesquisa-dois';
import { HttpModule } from '@angular/http';


const firebaseAuth = {
  apiKey: "AIzaSyDd3468R4YAWU-377dELXWT7hDRNvg9rBI",
  authDomain: "querofesta-cfd72.firebaseapp.com",
  databaseURL: "https://querofesta-cfd72.firebaseio.com",
  projectId: "querofesta-cfd72",
  storageBucket: "querofesta-cfd72.appspot.com",
  messagingSenderId: "13039723280"
};

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    CadastroEmailPage,
    EsqueciSenhaPage,
    PesquisaDoisPage
  ],
  imports: [
    BrowserModule,
    AngularFireAuthModule,
    IonicModule.forRoot(MyApp),
    AngularFireModule.initializeApp(firebaseAuth),
    InputMaskModule,
    HttpModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    CadastroEmailPage,
    EsqueciSenhaPage,
    PesquisaDoisPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    { provide: ErrorHandler, useClass: IonicErrorHandler },
    FileTransfer,
    Transfer,
    File,
    Camera
  ]
})
export class AppModule { }
